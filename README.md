# r2-stickytab-chrome-ext
Rancher v2 sticky tabs, hides [ Download YAML ] [ Delete ] buttons, includes styles.css as new css rule

# How to use it

- Clone repository or download `zip` an extract it.
- Open tab in Chrome browser to `chrome://extensions`.
- Activate developer mode
- Click load and select path to local directory

## TODO

- Add `options.ui` to alter `manifest.json` entry: `content_scripts.matches`
